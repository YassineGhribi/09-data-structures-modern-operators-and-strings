document.body.append(document.createElement("textarea"));
document.body.append(document.createElement("button"));
const text = document.querySelector("textarea").value;
/*
underscore_case
  first_name
Some_Variable
  calculate_AGE
delayed_departure
*/
document.querySelector("button").addEventListener("click", function () {
  const text = document.querySelector("textarea").value;
  console.log(text);

  const rows = text.split("\n");
  console.log(rows);
  for (const [i, row] of rows.entries()) {
    const [first, second] = row.toLowerCase().trim().split("_");
    const output = `${first}${second.replace(
      second[0],
      second[0].toUpperCase()
    )}`;
    console.log(`${output.padEnd(20)}${"✅".repeat(i + 1)}`);
  }
});
/*
const gameEvents = new Map([
  [17, "⚽️ GOAL"],
  [36, "🔁 Substitution"],
  [47, "⚽️ GOAL"],
  [61, "🔁 Substitution"],
  [64, "🔶 Yellow card"],
  [69, "🔴 Red card"],
  [70, "🔁 Substitution"],
  [72, "🔁 Substitution"],
  [76, "⚽️ GOAL"],
  [80, "⚽️ GOAL"],
  [92, "🔶 Yellow card"],
]);
//Question 1
const events = [...new Set(gameEvents.values())];
console.log(events);

//Question 2
gameEvents.delete(64);
console.log(gameEvents);

//Question 3
console.log(
  `An event happaned , an average, every ${90 / gameEvents.size} minutes`
);
const time = [...gameEvents.keys()].pop();
console.log(time);
console.log(
  `An event happaned , an average, every ${time / gameEvents.size} minutes`
);
//Question 4
for (const [min, event] of gameEvents) {
  const half = min <= 45 ? "fist" : "second";
  console.log(`[${half} HALF] ${min}:${event}`);
}

const game = {
  team1: "Bayern Munich",
  team2: "Borrussia Dortmund",
  players: [
    [
      "Neuer",
      "Pavard",
      "Martinez",
      "Alaba",
      "Davies",
      "Kimmich",
      "Goretzka",
      "Coman",
      "Muller",
      "Gnarby",
      "Lewandowski",
    ],
    [
      "Burki",
      "Schulz",
      "Hummels",
      "Akanji",
      "Hakimi",
      "Weigl",
      "Witsel",
      "Hazard",
      "Brandt",
      "Sancho",
      "Gotze",
    ],
  ],
  score: "4:0",
  scored: ["Lewandowski", "Gnarby", "lewandowski", "Hummels"],
  date: "Nov 9th, 2037",
  odds: {
    team1: 1.33,
    x: 3.25,
    team2: 6.5,
  },
};

//Question 1
for ([i, player] of game.scored.entries()) {
  console.log(`goal ${i + 1} ${player}`);
}

//Question 2
const odds = Object.values(game.odds);
let average = 0;
for (const odd of odds) average += odd;
average /= odds.length;
console.log(average);

//Question 3
for (const [team, odd] of Object.entries(game.odds)) {
  const teamstr = team === "x" ? "draw" : `victory ${game[team]}`;
  console.log(`Odd of ${teamstr} victory : ${odd}`);
}

// Question 1
const [players1, players2] = game.players;
console.log(players1, players2);

//Question 2
const [gk, ...fieldPlayers] = players1;
console.log(gk, fieldPlayers);

//Question 3
const allPlayers = [...players1, ...players2];
console.log(allPlayers);

//Question 4
const players1Final = [...players1, "Thiago", "Coutinho", "Perisic"];
console.log(players1Final);

//Question 5
// const { team1, x: draw, team2 } = game.odds;
const {
  odds: { team1, x: draw, team2 },
} = game;
console.log(team1, draw, team2);

//Question 6
const printGoals = function (...players) {
  console.log(players);
  console.log(`${players.length} goals were scored`);
};

printGoals(...game.scored);

//Question 7
team1 < team2 && console.log("team1 is more likely to win");
team2 < team1 && console.log("team2 is more likely to win");
*/
